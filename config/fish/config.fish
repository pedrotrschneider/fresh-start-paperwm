if status is-interactive
    # Commands to run in interactive sessions can go here
end

starship init fish | source

# Rust
set PATH $PATH ~/.cargo/bin

# Some non posix compliant stuff
bass source /etc/profile

# Ranger
alias ranger "ranger --choosedir=$HOME/.rangerdir && cd (cat ~/.rangerdir)"
alias r "ranger"

# Neofetch
alias neo="clear && neofetch"

# Pfetch
alias pf="clear && pfetch"

# Git
alias gitcl="git clone"
alias gitst="git status"
alias gitpl="git pull"
alias gits="git status"
alias gita="git add"
alias gitc="git commit"
alias gitcm="git commit -m"
alias gitps="git push"
alias gitch="git checkout"

# lsd
alias ls="lsd"
alias ll="lsd -l"
alias la="lsd -la"
