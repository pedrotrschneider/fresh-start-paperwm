# Application shortcuts
# Launch terminal emulator
super + Return 
	black-box

# Launch web browser
super + b
	firefox

# Launch sound settings
super + v
	pavucontrol

# Launch file browser
super + n
	nautilus

# Launch spotify
super + s
	flatpak run com.spotify.Client

# SXHKD shortcuts
# Reload sxhkd
super + h; super + r
	killall sxhkd && sxhkd &

# Window manager shortcuts
# Switch to workspaace by number
super + {1,2,3,4,5,6,7,8,9}
	wmctrl -s {0,1,2,3,4,5,6,7,8}

# Switch to workspaces by keypad numbers
super + {KP_End,KP_Down,KP_Next,KP_Left,KP_Begin,KP_Right,KP_Home,KP_Up,KP_Prior}
	wmctrl -s {0,1,2,3,4,5,6,7,8}

# Move active window to workspace by number
super + shift + {1,2,3,4,5,6,7,8,9}
	move-active-window {0,1,2,3,4,5,6,7,8}

# Move active window to workspace by keypad number
super + shift + {KP_End,KP_Down,KP_Next,KP_Left,KP_Begin,KP_Right,KP_Home,KP_Up,KP_Prior}
	move-active-window {0,1,2,3,4,5,6,7,8}

# Switch to the next (right) workspace
super + ctrl + Up
	xdotool set_desktop --relative +1

# Switch to the previuous (left) workspace
super + ctrl + Down
	xdotool set_desktop --relative -- -1

# Move active window to next (right) workspace
super + shift + Up
	move-active-window-relative +1

# Move active window to previous (left) workspace
super + shift + Down
	move-active-window-relative -1

# Maximize active window
super + m
	wmctrl -r :ACTIVE: -b toggle,maximized_vert,maximized_horz

# Fullscreen active window
super + f
	wmctrl -r :ACTIVE: -b toggle,fullscreen

# Kill active window
super + q
	wmctrl -c :ACTIVE:


# Session control shortcuts
# Lock
super + l
	sleep 0.2 && dbus-send --type=method_call --dest=org.gnome.ScreenSaver \
	          /org/gnome/ScreenSaver org.gnome.ScreenSaver.Lock
	
# Logout
super + x; super + l
	sleep 0.2 && gnome-session-quit

# Reboot
super + x; super + r
	sleep 0.2 && gnome-session-quit --reboot

# Shutdown
super + x; super + s
	sleep 0.2 && gnome-session-quit --power-off
