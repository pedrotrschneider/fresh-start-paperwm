# Fresh Start Fedora 37

This repository expects to be in the folder ```~/.src/fresh-start-paperwm```. If you are unsure of how to do this, run these commands to clone the repo:

```bash
mkdir -p ~/.src/
cd ~/.src/
git clone https://gitlab.com/pedrotrschneider/fresh-start-paperwm.git
```

If you wish to clone the repo to a different folder, make sure to edit all the scripts accordingly.

## Post installation

Start by editing ```/etc/dnf/dnf.conf``` by adding these lines at the end

```bash
fastestmirror=True
max_parallel_downloads=10
defaultyes=True
keepcache=True
```

Then, update your system

```bash
sudo dnf update
```

Enable free and non-free repositories on rpm fusion

```bash
sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm -y
sudo dnf install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm -y
```

Add flathub source to flatpaks

```bash
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
```

Change hostname

```bash
sudo hostnamectl set-hostname "paul"
```

Install media codecs

```bash
sudo dnf groupupdate multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin -y
sudo dnf groupupdate sound-and-video -y
```

Now, to setup Git, set username and email

```bash
git config --global user.name "Pedro Tonini Rosenberg Schneider"
git config --global user.email "pedrotrschneider@gmail.com"
```

Generate ssh keys

```bash
ssh-keygen -t ed25519
```

Then add the ssh keys to GitHub and GitLab

Install some binaries

```bash
mkdir -p ~/.local/bin/
cd ~/.src/fresh-start-paperwm/bin/
cp * ~/.local/bin/
```

Copy ```.desktop``` files

```bash
mkdir -p ~/.local/share/applications/
cd ~/.src/fresh-start-paperwm/applications/
cp * ~/.local/share/applications/
```

Copy some program binaries

```bash
cd ~/.src/fresh-start-paperwm/
cp -r programs ~/Documents/
```

Copy config files 

```bash
cd ~/.src/fresh-start-paperwm/config/
cp -r fish ~/.config/
cp -r micro ~/.config/
cp -r ranger ~/.config/
cp -r sayonara ~/.config/
cp -r xournalpp ~/.config/
cp -r sxhkd ~/.config/
```

Now set the number of workspaces to a fixed 9, and make workspaces span all monitors (this is on settings > multitasking).

## Installing applications

### Installing [pfetch](https://github.com/dylanaraps/pfetch)

Clone the repo and install the application

```
mkdir -p ~/.local/bin/
cd ~/.src/
git clone https://github.com/dylanaraps/pfetch.git
cd pfetch/
ln -sri pfetch ~/.local/bin/
```

### Installing [Digimend Kernel Drivers](https://github.com/DIGImend/digimend-kernel-drivers)

Install dependencies

```bash
sudo dnf install -y "kernel-devel-uname-r == $(uname -r)"
sudo dnf install -y dkms
```

Clone and build the package

```bash
cd ~/.src
git clone https://github.com/DIGImend/digimend-kernel-drivers.git digimend-kernel-drivers
cd digimend-kernel-drivers
sudo make dkms_install
```

### Install VSCode

Add the repository to dnf

```bash
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
sudo dnf check-update
```

Install VSCode

```bash
sudo dnf install code -y
```

### Install the Starship Prompt

```bash
sudo dnf copr enable atim/starship -y
sudo dnf install starship -y
starship preset nerd-font-symbols > ~/.config/starship.toml
```

### Install Zero Tier One

Install the CLI tool

```bash
sudo dnf copr enable atim/zerotier-one -y
sudo dnf install zerotier-one -y
```

Start the service 

```bash
sudo zerotier-one -d
```

Join your network

```bash
sudo zerotier-cli join <network-id>
```

To leave a network you can use

```bash
sudo zerotier-cli leave <network-id>
```

### Install from package managers

Installing from dnf

```bash
sudo dnf install ranger xournalpp neofetch gnome-tweaks fish micro pavucontrol nemo nemo-fileroller nemo-image-converter krita blender xclip mpv htop cava fira-code-fonts bat exa playerctl lsd xdg-user-dirs pipewire pipewire-pulseaudio unzip lm_sensors pipewire-utils python3-pip python3-devel nautilus-image-converter sushi touchegg xdotool -y
```

Remember to edit nemo.desktop to make it appear on the overview.7

Enable the touchégg service and start it

```bash
sudo systemctl start touchegg
sudo systemctl enable touchegg
```

Installing from flathub

```bash
flatpak install flathub com.raggesilver.BlackBox -y
flatpak install flathub com.mattjakeman.ExtensionManager -y
flatpak install flathub com.discordapp.Discord -y
flatpak install flathub com.spotify.Client -y
flatpak install flathub com.github.marktext.marktext -y
flatpak install flathub com.github.flxzt.rnote -y
flatpak install flathub org.telegram.desktop -y
flatpak install flathub io.github.realmazharhussain.GdmSettings -y
```

Set ```Black Box``` as the default terminal for Gnome and Cinnamon (for nemo)

```bash
gsettings set org.gnome.desktop.default-applications.terminal exec black-box
gsettings set org.cinnamon.desktop.default-applications.terminal exec black-box
```

Install the fisher plugin manager for fish

```bash
curl -sL https://git.io/fisher | source && fisher install jorgebucaran/fisher
```

Install the bass plugin because POSIX

```bash
fish
fisher install edc/bass
```

## GNOME Extensions

Install extensions dependencies

```bash
sudo dnf install libgda libgda-sqlite
```

- Aylur's Widgets
- Blur My Shell
- Caffeine
- Pano - Clipboard Manager
- EvalGJS (see installation instructions)
- Just Perfection
- Quick Settings Tweaker
- PaperWM (see installation instructions)
- User Themes
- X11 gestures
- Launch New Instance

### Install [PaperWM](https://github.com/paperwm/PaperWM)

Installation has to be done manually, since the installation script doesn't work:

```bash
cd ~/.local/share/gnome-shell/extensions
git clone https://github.com/paperwm/PaperWM.git paperwm@hedning:matrix.org
```

Change branch to the appropriate Gnome Shell version (gnome-42 for shell version 42 and develop for shell version 43).

Restart the shell and activate the extension.

### Install [EvalGJS extension](https://github.com/ramottamado/eval-gjs#installation)

Installation ahs to be done manually since it is not available in Gnome Extensions:

```bash
cd ~/.src/
git clone https://github.com/ramottamado/eval-gjs.git
cd eval-gjs
make install
```

Restart the shell and enable the extension.

### Load Gnome settings

```bash
cd ~/.src/fresh-start-paperwm/
dconf load /org/gnome/shell/extensions/ < config/gnome-extension-settings.conf
```

## Theming

Copy all wallpapers to the folders

```bash
mkdir -p ~/Pictures/backgrounds/
cd ~/.src/fresh-start-paperwm/backgrounds/
cp * ~/Pictures/backgrounds/
sudo cp * /usr/share/backgrounds/
```

Create the necessary folder:

```bash
mkdir -p ~/.themes/
mkdir -p ~/.icons/
mkdir -p ~/.config/gtk-4.0/
```

Override flatpak themes

```bash
sudo flatpak override --filesystem=xdg-config/gtk-4.0
sudo flatpak override --filesystem=/home/$USER/.icons/:ro
```

Install dependencies

```bash
sudo dnf install gtk-murrine-engine -y
```

Install Sauce Code Pro font

```bash
cd /tmp/
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/SourceCodePro.zip
mkdir -p ~/.local/share/fonts/
unzip SourceCodePro.zip -d ~/.local/share/fonts/
fc-cache ~/.local/share/fonts/
```

### Install [Nightfox Gtk Theme](https://github.com/Fausto-Korpsvart/Nightfox-GTK-Theme.git)

Clone the theme

```bash
cd ~/.src/
git clone https://github.com/Fausto-Korpsvart/Nightfox-GTK-Theme.git
```

Install it for the shell and gtk3

```bash
cd ~/.src/Nightfox-GTK-Theme/themes/
cp -r Nightfox-Dusk-BL/ ~/.themes/
cp -r Nightfox-Dusk-B/ ~/.themes/
sudo cp -r Nightfox-Dusk-B/ /usr/share/themes/
```

Install it for gtk4/libadwaita

```bash
cd ~/.src/Nightfox-GTK-Theme/themes/Nightfox-Dusk-BL/gtk-4.0/
cp -r * ~/.config/gtk-4.0/
```

Install the icon themes

```bash
cd ~/.src/Nightfox-GTK-Theme/Icons/
cp -r Duskfox-Alt/ ~/.icons/
sudo cp -r Duskfox-Alt/ /usr/share/icons/
```

Now, select the themes on Gnome Tweaks: ```Nightfox-Dusk-B``` for shell, ```Nightfox-Dusk-BL``` for legacy applications and ```Duskfox-alt``` for icons.

### Install [Simp1e Catpuccin Cursors](https://www.gnome-look.org/p/1932768)

Install the theme

```bash
cd ~/Downloads/
wget https://gitlab.com/cursors/simp1e/-/jobs/3269567407/artifacts/raw/built_themes/Simp1e-Catppuccin-Mocha.tgz
tar xfv Simp1e-Catppuccin-Mocha.tgz
mv Simp1e-Catppuccin-Mocha/ ~/.icons/
```

Now, select ```Simp1e-Catpuccin-Mocha``` for the cursor on Gnome Tweaks.

### Install [Gruvbox Gtk Theme](https://github.com/Fausto-Korpsvart/Gruvbox-GTK-Theme)

Clone the theme

```bash
cd ~/.src
git clone https://github.com/Fausto-Korpsvart/Gruvbox-GTK-Theme.git
```

Install it for shell and gtk3

```bash
cd ~/.src/Gruvbox-GTK-Theme/themes/
cp -r Gruvbox-Dark-B/ ~/.themes/
cp -r Gruvbox-Dark-BL/ ~/.themes/
sudo cp -r Gruvbox-Dark-B/ /usr/share/themes/
```

Install ir for gtk4/libadwaita

```bash
cd ~/.src/Gruvbox-GTK-Theme/themes/Gruvbox-Dark-BL/gtk-4.0/
cp -r * ~/.config/gtk-4.0/
```

Install the icon theme

```bash
cd ~/.src/Gruvbox-GTK-Theme/icons/
cp -r Gruvbox_Dark-2/ ~/.icons/
sudo cp -r Gruvbox_Dark-2/ /usr/share/icons/
```

Now, select the themes on Gnome Tweaks: ```Gruvbox-Dark-B``` for shell, ```Nightfox-Dusk-BL``` for legacy applications and ```Gruvbox-Dark2``` for icons.

### Install [Simp1e Gruvbox Cursors](https://www.gnome-look.org/p/1932768)

Install the theme

```bash
cd ~/Downloads/
wget https://gitlab.com/cursors/simp1e/-/jobs/3269567407/artifacts/raw/built_themes/Simp1e-Gruvbox-Dark.tgz
tar xfv Simp1e-Gruvbox-Dark.tgz
mv Simp1e-Gruvbox-Dark/ ~/.icons/
```

Now, select ```Simp1e-Gruvbox-Dark``` for the cursor on Gnome Tweaks.

## General keybindgs

In this section, you must choose your keybinding daemon based on your display server (Xorg or Wayland). If you want to use Xorg, install sxhkd. If you want to use Wayland, install swhkd

### Install [sxhkd](https://github.com/baskerville/sxhkd)

Install sxhkd from dnf 

```bash
sudo dnf install sxhkd -y
```

Add the sxhkd autostart from Gnome Tweaks.

Also, for some reason, I couldn't get the overview keybinding to work on sxhkd, so you need to add a keybinding from the Gnome settings pannel calling the executable `toggle-overview`.

### Install [swhkd](https://github.com/waycrate/swhkd)

Install dependencies

```bash
sudo dnf install scdoc -y
```

Install rust

```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

Log out and log back in.

Install swhkd

```bash
cd ~/.src/
git clone https://github.com/waycrate/swhkd
cd swhkd/
make setup
make clean
make
sudo make install
```

Copy the startup scritp to any bin path

```bash
cd ~/.src/swhkd/contrib/init/systemd/
mkdir -p ~/.local/bin/
cp hotkeys.sh ~/.local/bin/
```

Create a symlink from the config file to the default config file location

```bash
sudo ln -si ~/.src/fresh-start-paperwm/config/swhkdrc /etc/swhkd/swhkdrc
```

Open Gnome Tweaks and add SWHKD Autostart as an autostart application.

- ```sudo pkill -HUP swhkd``` to reload the config file

### Disable overview

1. Open gnome tweaks
2. Go to keyboard and mouse
3. Set overview shortcut to right super

### Disable some gnome shortcuts that interfere with global shortcuts

```bash
dconf write '/org/gnome/shell/keybindings/switch-to-application-1' "['']"
dconf write '/org/gnome/shell/keybindings/switch-to-application-2' "['']"
dconf write '/org/gnome/shell/keybindings/switch-to-application-3' "['']"
dconf write '/org/gnome/shell/keybindings/switch-to-application-4' "['']"
dconf write '/org/gnome/shell/keybindings/switch-to-application-5' "['']"
dconf write '/org/gnome/shell/keybindings/switch-to-application-6' "['']"
dconf write '/org/gnome/shell/keybindings/switch-to-application-7' "['']"
dconf write '/org/gnome/shell/keybindings/switch-to-application-8' "['']"
dconf write '/org/gnome/shell/keybindings/switch-to-application-9' "['']"
dconf write '/org/gnome/shell/keybindings/focus-active-notification' "['']"
dconf write '/org/gnome/shell/keybindings/toggle-message-tray' "['']"
dconf write '/org/gnome/shell/keybindings/toggle-overview' "['']"
dconf write '/org/gnome/shell/keybindings/toggle-application-view' "['']"
dconf write '/org/gnome/shell/keybindings/open-application-menu' "['']"
dconf write '/org/gnome/shell/keybindings/shift-overview-down' "['']"
```

## Configure [GDM Settings](https://github.com/realmazharhussain/gdm-settings)

It needs to be run from the terminal

```bash
flatpak run io.github.realmazharhussain.GdmSettings
```

Appearence tab:

- Shell: ```Nightfox-Dusk-B```
- Icons: ```Duskfox-Alt```
- Background: type image and select the appropriate background

Display tab:

- Apply current display configuration

Login Screen tab:

- Enable wellcome mesage: "Welcome, Pedro!"

If you are using a HiDPI display and want to make the login screen scale, edit the file `/usr/share/glib-2.0/schemas/org.gnome.desktop.interface.gschema.xml` with superuser privileges and change this property:

```xml
<key name="scaling-factor" type="u">
    <default>2</default>
```

The default value should be 0, wich means gdm will ignore all settings. Setting it to 2 will force gdm to use 200% display scaling. After editing, run this command:

```bash
glib-compile-schemas /usr/share/glib-2.0/schemas
```

## Edit Black Box's configuration file

Config file is located at ```~/.var/app/com.raggesilver.BlackBox/config/glib-2.0/settings/keyfile```

Change the contents of the file to

```
[com/raggesilver/BlackBox]
theme-dark='Dracula'
use-custom-command=true
custom-shell-command='fish'
terminal-padding=(20, 20, 20, 20)
easy-copy-paste=true
window-width=1108
window-height=1019
cursor-shape=1
show-scrollbars=true
pixel-scrolling=false
font='SauceCodePro Nerd Font Mono 12'
headerbar-draw-line-single-tab=false
```
